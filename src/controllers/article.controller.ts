import { Router } from 'express';
import { ArticleData } from '../datas/article.data';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Article } from '../models/article.model';
import { RestaurantModel } from '../models/restaurant.model';
import { Restaurant } from '~/datas/restaurant.data';

const articleController = Router();

const article = new ArticleData();

articleController.get('/:id_article', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var article_get: Article = new Article({
            id_article: req.params.id_article
        })

        article_get.IsIdArticleValid()
        
        var art = await article.getArticleByIdArticle(article_get);

        const message: string = "Get restaurant acticle by id_article"
        returnSuccess(res, art, message)

    } catch (err) {
        next(err)
    }
})

articleController.get('/all/:id_restaurateur', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var restaurant_get: RestaurantModel = new RestaurantModel({
            id_restaurateur: req.params.id_restaurateur
        })

        restaurant_get.IsIdRestaurateurValid()

        var art = await article.getAllArticlesByIdRestaurateur(restaurant_get);

        const message: string = "Get restaurant acticles by id_restaureteur"
        returnSuccess(res, art, message)

    } catch (err) {
        next(err)
    }
})


articleController.post('/add', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const newArticle = req.body.article;

        const articles = await article.addArticleToRestaurant(req.body.id_restaurateur, newArticle);

        const message: string = "Add restaurant acticle"
        returnCreated(res, message);
    } catch (err) {
        next(err);
    }
});

articleController.put('/update', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const update_article: Article = new Article({
            id_article: req.body.id_article,
            article_name: req.body.article_name,
            img: req.body.img,
            price: req.body.price,
            description: req.body.description,
        })

        update_article.IsIdArticleValid()

        await article.UpdateArticleByIdArticle(update_article);

        if (update_article.description === null ) {
            await article.UnsetArticleDescriptionByIdArticle(update_article)
        }
        if (update_article.img === null) {
            await article.UnsetArticleImgByIdArticle(update_article)
        }

        const message: string = "Update restaurant acticle by id_article"
        returnUpdated(res, message);
    } catch (err) {
        next(err);
    }
});

articleController.delete('/delete/:id_article',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const update_article: Article = new Article({
            id_article: req.params.id_article,
        })

        update_article.IsIdArticleValid()

        await article.DeleteArticleByIdArticle(update_article)

        const message: string = "Delete restaurant acticle by id_article"
        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

export { articleController };