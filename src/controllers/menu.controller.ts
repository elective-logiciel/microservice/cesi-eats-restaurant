import { Router } from 'express';
import { MenuData } from '../datas/menu.data';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Menu } from '../models/menu.model';
import { Article } from '../models/article.model';
import { RestaurantModel } from '../models/restaurant.model';
import { Restaurant } from '~/datas/restaurant.data';

const menuController = Router();

const menu = new MenuData();

menuController.get('/:id_menu', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var men = new Menu({
            id_menu: req.params.id_menu
        })

        men.IsIdMenuValid()

        var art = await menu.getMenuByIdMenu(men);

        const message: string = "Get restaurant menu by id_menu"
        returnSuccess(res, art, message)

    } catch (err) {
        next(err)
    }
})

menuController.get('/all/:id_restaurateur', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var restaurant = new RestaurantModel({
            id_restaurateur: req.params.id_restaurateur
        })

        restaurant.IsIdRestaurateurValid()

        var art = await menu.getAllMenusByIdRestaurateur(restaurant);

        const message: string = "Get restaurant acticle by id_restaurateur"
        returnSuccess(res, art, message)

    } catch (err) {
        next(err)
    }
})

menuController.post('/add', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const newMenu = req.body.menu; // Récupérer les informations du nouveau menu depuis la requête

        const menus = await menu.addMenuToRestaurant(req.body.id_restaurateur, newMenu);

        const message: string = "Add restaurant menu"
        returnCreated(res, menu);
        
    } catch (err) {
        next(err);
    }
});

menuController.put('/update', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const update_menu: Menu = new Menu({
            id_menu: req.body.id_menu,
            menu_name: req.body.menu_name,
            img: req.body.img,
            price: req.body.price,
            description: req.body.description,
        })

        update_menu.IsIdMenuValid()

        await menu.UpdateMenuByIdMenu(update_menu);

        if (update_menu.description === null) {
            await menu.UnsetMenueDescriptionByIdMenu(update_menu)
        }
        if (update_menu.img === null) {
            await menu.UnsetMenuImgByIdMenu(update_menu)
        }

        const message: string = "Update restaurant menu by id_menu"
        returnUpdated(res, menu);

    } catch (err) {
        next(err);
    }
});

menuController.delete("/delete/:id_menu", async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        var men: Menu = new Menu ({
            id_menu: req.params.id_menu
        })

        men.IsIdMenuValid()

        await menu.deleteMenuByIdMenu(men);

        const message: string = "Delete restaurant menu by id_menu"
        returnDeleted(res, message);

    } catch (err) {
        next(err);
    }
})

menuController.post("/article/add", async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const add_article: Article = new Article({
            article_name: req.body.article_name,
            img: req.body.img,
            price: req.body.price,
            description: req.body.description,
        })

        const add_menu: Menu = new Menu({
            id_menu: req.body.id_menu
        })

        add_article.IsArticleNameValid()
        add_article.IsPriceValid()

        add_menu.IsIdMenuValid()

        await menu.AddArticleToMenuByIdMenu(add_menu, add_article)

        const message: string = "Add article to restaurant menu by id_menu"
        returnCreated(res, message);

    } catch (err) {
        next(err);
    }
})


menuController.delete("/article/delete", async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const delete_article: Article = new Article({
            id_article: req.body.id_article,
        })

        delete_article.IsIdArticleValid()

        await menu.DeleteArticleToMenuByIdArticle(delete_article)

        const message: string = "Delete article to restaurant menu by id_menu"
        returnDeleted(res, message);

    } catch (err) {
        next(err);
    }
})

export { menuController };