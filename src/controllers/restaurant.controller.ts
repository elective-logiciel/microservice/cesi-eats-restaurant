import { Router } from 'express';
import { Restaurant } from '../datas/restaurant.data';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { NText } from 'mssql';

const restaurantController = Router();

const restaurant = new Restaurant();

restaurantController.get('/all', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const allRestaurant = await restaurant.getAll();

        const message: string = "Get all restaurants"
        returnSuccess(res, allRestaurant, message)

    } catch (err) {
        next(err)
    }
})

restaurantController.get('/:id_restaurateur', async function ( req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const resto = await restaurant.getRestaurantByIdRestaurateur(req.params.id_restaurateur)
        
        const message: string = "Get restaurant by id_restaurateur"
        returnSuccess(res, resto, message)

    } catch (err) {
        next(err)
    }
})

restaurantController.post('/add', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        await restaurant.AddRestaurant(req.body);
        
        const message: string = "Add restaurant"
        returnCreated(res, message)

    } catch (err) {
        next(err)
    }
})

restaurantController.delete('/delete/:id_restaurateur', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        await restaurant.deleteRestaurantByIdRestaurateur(req.params.id_restaurateur)
        
        const message: string = "Delete restaurant by id_restaurateur"
        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

restaurantController.put('/update', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        await restaurant.updateRestaurantByIdRestaurateur(req.body.id_restaurateur, req.body.restaurant_name, req.body.restaurant_img)
        
        const message: string = "Update restaurant by id_restaurateur"
        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

export { restaurantController };