import { Log } from "../models/log.model"
import { Logger } from "../services/logging.service"
const config = require('../config')

const logSvc = new Logger()

function returnSuccess(res, data, message) {
    var logSuccess: Log = new Log({
        status_name: "Ok",
        status: 200,
        message: message,
        service_name: config.service_name,
    })
    logSvc.info(logSuccess)

    res.status(200).json({
        name: 'OK',
        status: 200,
        data: data
    })
}

function returnCreated(res, message) {
    var logSuccess: Log = new Log({
        status_name: "Created",
        status: 201,
        message: message,
        service_name: config.service_name,
    })
    logSvc.info(logSuccess)

    res.status(201).json({
        name: 'Created',
        status: 201,
        message: "Ressource succesfully created."
    })
}

function returnUpdated(res, message) {
    var logSuccess: Log = new Log({
        status_name: "No Content",
        status: 204,
        message: message,
        service_name: config.service_name,
    })
    logSvc.info(logSuccess)

    res.status(204).json({
        name: 'No Content',
        status: 204,
        message: "Ressource succesfully updated."
    })
}

function returnDeleted(res, message) {
    var logSuccess: Log = new Log({
        status_name: "No Content",
        status: 204,
        message: message,
        service_name: config.service_name,
    })
    logSvc.info(logSuccess)

    res.status(204).json({
        name: 'No Content',
        status: 204,
        message: "Ressource succesfully deleted."
    })
}

export { returnSuccess, returnCreated, returnUpdated, returnDeleted }