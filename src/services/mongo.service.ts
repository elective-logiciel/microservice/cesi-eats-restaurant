import { connect, Schema, model, ObjectId } from 'mongoose';
import { Article } from '../models/article.model';
import { Menu } from '../models/menu.model';
import { RestaurantModel } from '../models/restaurant.model';


const config = require('../config')

const mongoDb = connect(config.mongo.server + config.mongo.database);

const ArticleSchema = new Schema<Article>({
    article_name: { type: String, required: true },
    price: { type: Number, required: true },
    img: { type: String, required: false },
    description: { type: String, required: false }
})

const MenuSchema = new Schema<Menu>({
    menu_name: { type: String, required: true },
    price: { type: Number, required: true },
    article: { type: [ArticleSchema], required: false },
    img: { type: String, required: false },
    description: { type: String, required: false }
})


const RestaurantSchema = new Schema<RestaurantModel>({
    id_restaurateur: { type: Number, required: true },
    restaurant_name: { type: String, required: true },
    restaurant_img: { type: String, required: false },
    menus: { type: [MenuSchema], required: true },
    articles: { type: [ArticleSchema], required: true }
})

export const Restaurants = model<RestaurantModel>('Restaurant', RestaurantSchema);

// const MongoDb = connect('mongodb+srv://catarinasilvaroriz:WBQZ4rV19aDE305X@cesieats.nx791a4.mongodb.net/CesiEats');