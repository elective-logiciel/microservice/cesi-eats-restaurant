import { Restaurants } from "../services/mongo.service";
import { Menu } from "../models/menu.model";
import { Article } from "../models/article.model";
import { RestaurantModel } from "../models/restaurant.model";

var mongoose = require('mongoose');

export class MenuData {
    
    public async getMenuByIdMenu(menu: Menu) {

        var restaurant = await Restaurants.findOne(
            { menus: { $elemMatch: { _id: menu.id_menu } }}
        );

        if (restaurant && restaurant.menus !== undefined) {
            var art = restaurant.menus.find((menus: any) => menus._id.toString() === menu.id_menu)
            return art
        }

        return null

    }

    public async getAllMenusByIdRestaurateur(restaurant: RestaurantModel) {
        const rest = await Restaurants.findOne({ id_restaurateur: restaurant.id_restaurateur });

        if (rest && rest.menus !== undefined) {
            const menu = rest.menus;
            return menu;
        }

        return null
    }

    public async addMenuToRestaurant(id_restaurateur: number, menu: any) {
        try {
            const updatedRestaurant = await Restaurants.findOneAndUpdate(
                { id_restaurateur: id_restaurateur },
                { $push: { menus: menu } },
                { new: true }
            );

            if (updatedRestaurant && updatedRestaurant.menus !== undefined) {
                return updatedRestaurant.menus;
            }

            return []; // Retourner un tableau vide si le restaurant n'est pas trouvé ou si l'array menus est indéfini
        } catch (error) {
            throw error;
        }
    }

    public async UpdateMenuByIdMenu(menu: Menu) {
        // return null;
        const filter = { 'menus._id': menu.id_menu };
        const update = {
            '$set': {
                'menus.$.menu_name': menu.menu_name,
                'menus.$.price': menu.price,
                'menus.$.img': menu.img,
                'menus.$.description': menu.description
            },
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true }); 
    }

    public async UnsetMenueDescriptionByIdMenu(menu: Menu) {
        const filter = { 'menus._id': menu.id_menu };
        const update = {
            '$unset': {
                'menus.$.description': ""
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }

    public async UnsetMenuImgByIdMenu(menu: Menu) {
        const filter = { 'menus._id': menu.id_menu };
        const update = {
            '$unset': {
                'menus.$.img': ""
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }

    public async deleteMenuByIdMenu(men: Menu) {

        const filter = { 'menus._id': men.id_menu };
        const update = {
            '$pull': {
                'menus': {
                    '_id': men.id_menu
                }
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }

    public async AddArticleToMenuByIdMenu(menu: Menu, article: Article) {
        const filter = { 'menus._id': menu.id_menu };
        const update = {
            '$push': {
                'menus.$.article': article
            }
        };

        await Restaurants.findOneAndUpdate(filter, update);
    }

    public async DeleteArticleToMenuByIdArticle(article: Article) {
        const filter = { 'menus.article._id': article.id_article };
        const update = {
            '$pull': {
                "menus.$.article": { 
                    _id: article.id_article 
                }
            }
        };

        var res = await Restaurants.findOneAndUpdate(filter, update);
    }
}