import { Restaurants } from "../services/mongo.service";

export class Restaurant {

    public getAll() {
        return Restaurants.find();
    }

    public getRestaurantByIdRestaurateur(id_restaurateur: Number) {
        var restaurant = Restaurants.find({ "id_restaurateur": id_restaurateur })
        return restaurant;
    }

    public async deleteRestaurantByIdRestaurateur(id_restaurateur: Number) {
        await Restaurants.deleteOne({ "id_restaurateur": id_restaurateur })
    }

    public async updateRestaurantByIdRestaurateur(id_restaurateur: Number, name: string | null, img: string | null) {

        const updateData: { restaurant_name?: string, restaurant_img?: string } = {};

        if (name) {
            updateData.restaurant_name = name;
        }

        if (img) {
            updateData.restaurant_img = img;
        }

        await Restaurants.updateOne({ "id_restaurateur": id_restaurateur }, { $set: updateData })

    }

    public AddRestaurant(body: any) {
        var article = new Restaurants(body)
        article.save()
    }
}