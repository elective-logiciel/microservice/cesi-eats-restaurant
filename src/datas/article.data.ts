import { Restaurants } from "../services/mongo.service";
import { Article } from "../models/article.model";
import { RestaurantModel } from "../models/restaurant.model";
import { ListFormat } from "typescript";

export class ArticleData {

    public async getArticleByIdArticle(article: Article) {
        var restaurant = await Restaurants.findOne({ 
            articles: {
                $elemMatch: { 
                    _id: article.id_article 
                }
            }
        })

        if (restaurant && restaurant.articles !== undefined) {
            var art = restaurant.articles.find(
                (articles: any) => articles._id.toString() === article.id_article)
            return art
        }

        return null
    }

    public async getAllArticlesByIdRestaurateur(restaurant_get: RestaurantModel) {
        const restaurant = await Restaurants.findOne({ id_restaurateur: restaurant_get.id_restaurateur });

        if (restaurant && restaurant.articles !== undefined) {
            const articles = restaurant.articles;
            return articles;
        }

        return null
    }

    public async addArticleToRestaurant(id_restaurateur: number, article: any) {
        const updatedRestaurant = await Restaurants.findOneAndUpdate(
            { id_restaurateur: id_restaurateur },
            { $push: { articles: article } },
            { new: true }
        );

        if (updatedRestaurant && updatedRestaurant.articles !== undefined) {
            return updatedRestaurant.articles;
        }

        return null;
    }

    public async UpdateArticleByIdArticle(article: Article) {
        const filter = { 'articles._id': article.id_article };
        const update = { 
            '$set': { 
                'articles.$.article_name': article.article_name,
                'articles.$.price': article.price,
                'articles.$.img': article.img,
                'articles.$.description': article.description
            },
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true }); 

    }

    public async UnsetArticleDescriptionByIdArticle(article: Article) {
        const filter = { 'articles._id': article.id_article };
        const update = {
            '$unset': {
                'articles.$.description': ""
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }

    public async UnsetArticleImgByIdArticle(article: Article) {
        const filter = { 'articles._id': article.id_article };
        const update = {
            '$unset': {
                'articles.$.img': ""
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }

    public async DeleteArticleByIdArticle(article: Article) {
        const filter = { 'articles._id': article.id_article };
        const update = {
            '$pull': {
                'articles': {
                    '_id': article.id_article
                }
            }
        };

        await Restaurants.findOneAndUpdate(filter, update, { $merge: true });
    }
}