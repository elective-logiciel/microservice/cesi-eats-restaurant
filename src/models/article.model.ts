import { Error400 } from "../errors/errors"

export class Article {
    id_article?: string
    article_name?: string
    price?: number
    img?: string | null
    description?: string | null

    public constructor(init?: Partial<Article>) {
        Object.assign(this, init);
    }

    public IsIdArticleValid(): boolean {
        if (this.id_article === undefined) {
            throw new Error400("Missing argument, 'id_article' can not be NULL")
        }
        if (this.id_article === "") {
            throw new Error400("Empty argument, 'id_article' can not be EMPTY")
        }
        return true
    }

    public IsArticleNameValid(): boolean {
        if (this.article_name === undefined) {
            throw new Error400("Missing argument, 'article_name' can not be NULL")
        }
        if (this.article_name === "") {
            throw new Error400("Empty argument, 'article_name' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Argument, 'price' can not be negative")
        }
        return true
    }

    public IsImageValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }
}

// Convert the result send by the database into a array of user struct
// export function convertQueryResToAddressList(query_res: any): Articles[] {
//     let article_list: Articles[] = []

//     for (let i = 0; i < query_res.rowsAffected[0]; i++) {
//         let res_article: any = query_res.recordset[i]

//         article_list.push(new Articles({
//             id_address: res_address.Id_Address,
//             street: res_address.Street,
//             zip_code: res_address.Zip_Code,
//             city: res_address.City,
//             id_user: res_address.Id_User,
//         }))
//     }

//     return address_list
// }
