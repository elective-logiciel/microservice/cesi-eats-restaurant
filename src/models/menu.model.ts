import { Types } from "mongoose"
import { Error400 } from "../errors/errors"
import { Article } from "./article.model"

export class Menu {
    id_menu?: string
    menu_name?: string
    price?: number
    article?: Array<Article>
    img?: string | null
    description?: string | null

    public constructor(init?: Partial<Menu>) {
        Object.assign(this, init);
    }

    public IsIdMenuValid(): boolean {
        if (this.id_menu === undefined) {
            throw new Error400("Missing argument, 'id_menu' can not be NULL")
        }
        if (this.id_menu === "") {
            throw new Error400("Empty argument, 'id_menu' can not be EMPTY")
        }
        return true
    }

    public IsMenuNameValid(): boolean {
        if (this.menu_name === undefined) {
            throw new Error400("Missing argument, 'menu_name' can not be NULL")
        }
        if (this.menu_name === "") {
            throw new Error400("Empty argument, 'menu_name' can not be EMPTY")
        }
        return true
    }

    public IsPriceValid(): boolean {
        if (this.price === undefined) {
            throw new Error400("Missing argument, 'price' can not be NULL")
        }
        if (this.price < 0) {
            throw new Error400("Argument, 'price' can not be negative")
        }
        return true
    }

    public IsImageValid(): boolean {
        if (this.img === undefined) {
            throw new Error400("Missing argument, 'img' can not be NULL")
        }
        if (this.img === "") {
            throw new Error400("Empty argument, 'img' can not be EMPTY")
        }
        return true
    }

    public IsDescriptionValid(): boolean {
        if (this.description === undefined) {
            throw new Error400("Missing argument, 'description' can not be NULL")
        }
        if (this.description === "") {
            throw new Error400("Empty argument, 'description' can not be EMPTY")
        }
        return true
    }
}