import { Error400 } from "../errors/errors"
import { Article } from "./article.model";
import { Menu } from "./menu.model";
import { Product } from "./product.model";

export class RestaurantModel {
    id_restaurateur?: number
    restaurant_name?: string
    restaurant_img?: string
    articles?: Array<Article>
    menus?: Array<Menu>

    public constructor(init?: Partial<RestaurantModel>) {
        Object.assign(this, init);
    }

    public IsIdRestaurateurValid(): boolean {
        if (this.id_restaurateur === undefined) {
            throw new Error400("Missing argument, 'id_restaurateur' can not be NULL")
        }
        if (this.id_restaurateur < 0) {
            throw new Error400("Out of range argument, 'id_restaurateur' can not be a negative number")
        }
        return true
    }

    public IsRestaurantNameValid(): boolean {
        if (this.restaurant_name === undefined) {
            throw new Error400("Missing argument, 'restaurant_name' can not be NULL")
        }
        if (this.restaurant_name === "") {
            throw new Error400("Empty argument, 'restaurant_name' can not be EMPTY")
        }
        return true
    }
}