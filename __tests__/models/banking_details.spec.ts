// import { Error400 } from "../../src/errors/errors"
// import { Banking_Details } from "../../src/models/banking_details.model";

// const valid_banking_details: Banking_Details = new Banking_Details({
//     id_user: 1,
//     rib: 'rib'
// })

// const unvalid_banking_details: Banking_Details = new Banking_Details({
//     id_user: -1,
//     rib: '',
// })

// const undefined_Banking_details: Banking_Details = new Banking_Details({})

// describe('User structure field are correct', () => {
//     test('Id User is set and valid', () => {
//         const user: Banking_Details = valid_banking_details

//         expect(user.IsIdUserValid()).toBeTruthy()
//     })

//     test('Rib is set and valid', () => {
//         const user: Banking_Details = valid_banking_details

//         expect((user.IsRibValid())).toBeTruthy()
//     })
// })

// describe('User structure field are undefined', () => {
//     test('Id User is set and unvalid', () => {
//         const user: Banking_Details = unvalid_banking_details
//         const err: Error400 = new Error400("Out of range argument, 'id_user' can not be a negative number")

//         function caughtError() {
//             user.IsIdUserValid()
//         }

//         expect(caughtError).toThrowError(err)
//     })

//     test('Rib is set and unvalid', () => {
//         const user: Banking_Details = unvalid_banking_details
//         const err: Error400 = new Error400("Empty argument, 'rib' can not be EMPTY")

//         function caughtError() {
//             user.IsRibValid()
//         }

//         expect(caughtError).toThrow(err)
//     })
// })

// describe('User structure field are undefined', () => {
//     test('Id User is not set and undefined', () => {
//         const user: Banking_Details = undefined_Banking_details
//         const err: Error400 = new Error400("Missing argument, 'id_user' can not be NULL")

//         function caughtError() {
//             user.IsIdUserValid()
//         }

//         expect(caughtError).toThrow(err)
//     })

//     test('Rib is not set and undefined', () => {
//         const user: Banking_Details = undefined_Banking_details
//         const err: Error400 = new Error400("Missing argument, 'rib' can not be NULL")

//         function caughtError() {
//             user.IsRibValid()
//         }

//         expect(caughtError).toThrow(err)
//     })
// })
